<?php
/**
 * Finding empty slot in calendar
 * @author Mateusz Rohde <mateusz.rohde@gmail.com>
 * @version 1.0
 */
define('NEWLINE', php_sapi_name() == 'cli' ? "\n" : "<br/>");
class fesic{	
	/**
	 * Meeting length (seconds)
	 * @var integer
	 */
	var $length;
	/**
	 * Number of possible time-slots that should be found by program.
	 * @var integer
	 */
	var $timeSlotsNumber;
	/**
	 * Longer time period in which we want to find empty slots in calendar. (start) 
	 * @var string
	 */
	var $timeFrameStart;
	/**
	 * Longer time period in which we want to find empty slots in calendar. (end) 
	 * @var string
	 */
	var $timeFrameEnd;
	/**
	 * Attendees data
	 * @var SimpleXMLElement
	 */
	var $attendees;
	/**
	 * attendees names
	 * @var array:string
	 */
	var $names;
	/**
	 * Found time slots
	 * @var array
	 */
	var $timeSlots;
	/**
	 * minimum work time start in db
	 * @var int
	 */
	var $minTime;
	/**
	 * maximum work time start in db
	 * @var int
	 */
	var $maxTime;
	/**
	 * number of found time slots
	 * @var int
	 */
	var $foundSlots;
	
	function __construct(){
		set_error_handler(array($this, 'handleError'));
		try{
			$this->loadArgs();
			$this->loadAttendees();
			$results = $this->findTimeSlots();
			$this->showResults($results);
		}
		catch(ErrorException $e){
			echo "An error occured:".NEWLINE.$e->getMessage();
		}
	}
	/**
	 * print results
	 * @param array $results
	 */
	public function showResults(array $results){
		foreach($results as $k => $v){
			$k = explode("|", $k);
			echo "Date: ".date("Y-m-d H:i", $k[0])." - ".date("Y-m-d H:i", $k[1])." - ";
			if(count($v) == count($this->attendees)){
				echo "Everyone can attend the meeting at this time.".NEWLINE;
			}else{
				echo implode(", ", $v)." can attend the meeting at this time. ";
				$notAvail = array();
				foreach($this->names as $n){
					if(!in_array($n, $v)){
						$notAvail[] = $n;
					}
				}
				echo implode(", ", $notAvail)." ".(count($notAvail) == 1 ? "is" : "are")." not available.".NEWLINE;
			}
		}
	}	
	/**
	 * find available time slots
	 * @return array
	 */
	public function findTimeSlots(){
		$results = array();
		$this->foundSlots = 0;
		$startFrame = strtotime($this->timeFrameStart) < $this->minTime ? $this->minTime : strtotime($this->timeFrameStart);
		$endFrame = strtotime($this->timeFrameStop) > $this->maxTime ? $this->maxTime : strtotime($this->timeFrameStop);
		$i = $startFrame;
		$finish = false;
		while($i+$this->length<=$endFrame){
			$start = $i;
			$end = $i+$this->length;
			$i += $this->accuracy*60;
			$res = $this->attendees->xpath("//wh[@start<=$start][@end>=$end]");
			foreach($res as $r){
				$p = $r->xpath("../..");
				$bs = $p[0]->bookedSlots->xpath("bs[@start>=$start][@start<$end] | bs[@end>$start][@end<=$end] | bs[@start<$start][@end>$end]");
				if(empty($bs)){
					if(!isset($results["$start|$end"]))
						$results["$start|$end"] = array();
					$results["$start|$end"][] = (string)$p[0]->name;
					if(count($this->attendees) == count($results["$start|$end"])){
						$this->foundSlots++;
						if($this->foundSlots==$this->timeSlotsNumber){
							$finish = true;
							break;
						}
					}
				}
			}
			if($finish){
				break;
			}
		}
		uasort($results, function($a,$b){
			return count($b)-count($a);
		});
		return array_slice($results,0,$this->timeSlotsNumber);
	}	
	/**
	 * Load attendees data from xml and convert dates to timestamps
	 * @throws ErrorException
	 */
	public function loadAttendees(){
		if(file_exists(dirname(__FILE__).'/attendees.xml')){
			$this->maxTime = 0;
			$this->minTime = 999999999999999999;
			$this->attendees = simplexml_load_file(dirname(__FILE__).'/attendees.xml');
			foreach($this->attendees as $a){
				foreach($a->workinghours->wh as $wh){
					$wh->attributes()->start = strtotime($wh->attributes()->start);
					if((int)$wh->attributes()->start < $this->minTime)
						$this->minTime = (int)$wh->attributes()->start;
					$wh->attributes()->end = strtotime($wh->attributes()->end);
					if((int)$wh->attributes()->end > $this->maxTime)
						$this->maxTime = (int)$wh->attributes()->end;
				}
				foreach($a->bookedSlots->bs as $bs){
					$bs->attributes()->start = strtotime($bs->attributes()->start);
					$bs->attributes()->end = strtotime($bs->attributes()->end);
				}
			}
			$names = $this->attendees->xpath("//name");
			foreach($names as $n){
				$this->names[] = (string)$n;
			}
		}else{
			throw new ErrorException("'attendees.xml' does not exist!");
		}
	}
	/**
	 * load parameters
	 * @throws ErrorException
	 */
	private function loadArgs(){
		header("Content-type: text/html; charset=utf-8");
		//
		if(isset($_GET['length']) && preg_match("|^[0-9]+$|", $_GET['length'])){
			$this->length = $_GET['length'];
		}else{
			throw new ErrorException("'length' parameter is not set or has bad format (should be an integer).");
		}
		if(isset($_GET['timeSlotsNumber']) && preg_match("|^[0-9]+$|", $_GET['timeSlotsNumber'])){
			$this->timeSlotsNumber = $_GET['timeSlotsNumber'];
		}else{
			throw new ErrorException("'timeSlotsNumber' parameter is not set or has bad format (should be an integer).");
		}
		if(isset($_GET['timeFrameStart']) && preg_match("|^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$|", $_GET['timeFrameStart'])){
			$this->timeFrameStart = $_GET['timeFrameStart'];
		}else{
			throw new ErrorException("'timeFrameStart' parameter is not set or has bad format (YYYY-mm-dd H:i).");
		}
		if(isset($_GET['timeFrameStop']) && preg_match("|^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$|", $_GET['timeFrameStop'])){
			$this->timeFrameStop = $_GET['timeFrameStop'];
		}else{
			throw new ErrorException("'timeFrameStop' parameter is not set or has bad format (YYYY-mm-dd H:i).");
		}
		if(isset($_GET['accuracy']) && preg_match("|^[0-9]+$|", $_GET['accuracy'])){
			$this->accuracy = $_GET['accuracy'];
		}else if(isset($_GET['accuracy'])){
			throw new ErrorException("'accuracy' parameter must be an integer (minutes)!");
		}else{
			$this->accuracy = 60;
		}
		if(strtotime($this->timeFrameStart)>strtotime($this->timeFrameStop))
			throw new ErrorException("'timeFrameStop' must be less than 'timeFrameStart'!");
	}
	/**
	 * error handling
	 * @param unknown $errno
	 * @param unknown $errstr
	 * @param unknown $errfile
	 * @param unknown $errline
	 * @param string $errcontext
	 * @throws ErrorException
	 * @return boolean
	 */
	public function handleError($errno, $errstr, $errfile, $errline, $errcontext=null){
		if(0 === error_reporting() || preg_match('|mpdf|',$errfile))
			return false;
		throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
	}
}
new fesic();
?>